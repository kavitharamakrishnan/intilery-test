# Intilery Java Test

## TL;DR
1. Init a git repository, commit often
2. Run `./mvnw verify`
3. Read through the code, understand the structure
4. Make the health check pass
5. Make the e-commerce tests pass using the Graph repository for the data
6. Make the process tests pass
7. Ensure all changed are committed
8. `./mvnw clean`
9. Zip the contents of the directory
10. Send back

## Getting going

The following project requires that you have a Java 8 SDK and git installed.

Use git (there is a .gitignore file included), get started with `git init; git add .; git commit -m "Initial commit"`.
Commit as often as you see fit.

You can then start building and running the project using: `./mvnw verify`.

This will download all the dependencies, build the jar file, run any acceptance tests. There is one set up
that checks that the `greeter` component works.

Tests are written using [cucumber] tests in the `src/test/resources/features` directory. Make all the tests pass.

Look at the example `greeter` and follow the package structure for your classes.
 
You can run a single test by annotating the feature with `@wip` (work in progress) and running `./mvnw integration-test -Pwip`.

The first test to make pass is already marked as `@wip`.

When the test passes, remove the `@wip` tag and run `./mvnw verify` to make sure everything still works.

Then find the next test and change the `@pending` to `@wip` for that test.

## Finding tests and requirements...

Each requirement is described using a cucumber feature.

* Each feature is described in the `src/test/resources/features` directory
* Expected responses are in `src/test/resources/json`

## Running...

You run the application by calling the `com.intilery.exercise.web.ExerciseApplication#main`.

You can build a jar file with `./mvnw package -DskipTests`, then run it with `java -jar build/libs/com.intilery.exercise-0.1.0.jar`,
alternatively use `./mvnw spring-boot:run` to use the spring boot runner (the terminal will hang whilst this runs, stop with `CTRL-C`).

You can see the running application at `http://localhost:8080/hello-world`.

## Using IntelliJ
Create an IntelliJ project by running opening the project using the `pom.xml` file.

To run the application, run the main method listed above.

Install the __cucumber__ plugin to enable navigation through the features or running the features easier from within the IDE.



