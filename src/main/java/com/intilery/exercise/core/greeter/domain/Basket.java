package com.intilery.exercise.core.greeter.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Basket {

	private String name;
	private String image;
	private double price;
	private int quantity;

	public String getName() {
		return name;
	}
	public Basket withName(String name) {
		this.name = name;
		return this;
	}
	public String getImage() {
		return image;
	}
	public Basket withImage(String image) {
		this.image = image;
		return this;
	}
	public double getPrice() {
		return price;
	}
	public Basket withPrice(double price) {
		this.price = price;
		return this;
	}
	@JsonProperty("qty")
	public int getQuantity() {
		return quantity;
	}
	public Basket withQuantity(int quantity) {
		this.quantity = quantity;
		return this;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Basket other = (Basket) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
