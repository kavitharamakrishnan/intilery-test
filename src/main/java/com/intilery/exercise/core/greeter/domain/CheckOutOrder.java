package com.intilery.exercise.core.greeter.domain;

import com.tinkerpop.blueprints.Vertex;

public class CheckOutOrder {
	private Vertex previousCheckOutOrder;
	private Vertex currentCheckOutOrder;

	public Vertex getPreviousCheckOutOrder() {
		return previousCheckOutOrder;
	}
	public CheckOutOrder withPreviousCheckOutOrder(Vertex previousCheckOutOrder) {
		this.previousCheckOutOrder = previousCheckOutOrder;
		return this;
	}
	public Vertex getCurrentCheckOutOrder() {
		return currentCheckOutOrder;
	}
	public CheckOutOrder withCurrentCheckOutOrder(Vertex currentCheckOutOrder) {
		this.currentCheckOutOrder = currentCheckOutOrder;
		return this;
	}

}
