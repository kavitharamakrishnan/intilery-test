package com.intilery.exercise.core.greeter.domain;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerBasket {

	private String emailId;
	private List<Basket> basket = new ArrayList<Basket>();

	@JsonProperty("email")
	public String getEmailId() {
		return emailId;
	}
	public CustomerBasket withEmailId(String emailId) {
		this.emailId = emailId;
		return this;
	}
	public List<Basket> getBasket() {
		return basket;
	}
	public CustomerBasket withBasket(List<Basket> basket) {
		this.basket = basket;
		return this;
	}


}
