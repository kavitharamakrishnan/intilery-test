package com.intilery.exercise.core.greeter.domain;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Order {

	private String orderId;
	private List<Line> lines = new ArrayList<Line>();

	@JsonProperty("orderID")
	public String getOrderId() {
		return orderId;
	}
	public Order withOrderId(String orderId) {
		this.orderId = orderId;
		return this;
	}
	@Override
	public String toString() {
		return "Order [orderId=" + orderId + "]";
	}
	public List<Line> getLines() {
		return lines;
	}
	public Order withLines(List<Line> lines) {
		this.lines = lines;
		return this;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		return true;
	}


}
