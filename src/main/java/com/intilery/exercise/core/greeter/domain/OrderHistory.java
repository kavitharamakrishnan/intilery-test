package com.intilery.exercise.core.greeter.domain;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderHistory {

	private String emailId;
	private List<Order> orders = new ArrayList<Order>();

	@JsonProperty("email")
	public String getEmailId() {
		return emailId;
	}
	public OrderHistory withEmailId(String emailId) {
		this.emailId = emailId;
		return this;
	}
	@JsonProperty("orders")
	public List<Order> getOrders() {
		return orders;
	}
	@JsonProperty("order")
	public Order getOrder() {
		return orders.get(0);
	}
	public OrderHistory withOrders(List<Order> orders) {
		this.orders = orders;
		return this;
	}

}
