package com.intilery.exercise.core.greeter.health;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;

@Endpoint(id = "greeterHealth")
@Component
public class GreetingCustomHealthCheck {

	private Client client;
	private final String GREETING_URL = "http://localhost:8080/hello-world";
	private final String EXPECTED_RESPONSE = "{\"greeting\":\"Hello World!\"}";

	@ReadOperation
	public String getHealth() {
		try {
			client = ClientBuilder.newClient();
			Invocation invocation = client.target(GREETING_URL).request(MediaType.APPLICATION_JSON_TYPE).buildGet();
			Response response = invocation.invoke();
			if (response == null) {
				return constructHealthDownWithReason("HealthCheck response is null");
			} else if (Status.OK.getStatusCode() != response.getStatus()) {
				return constructHealthDownWithReason(
						String.format("%s : %s", "Error status code is", response.getStatus()));
			} else {
				String responseContent = response.readEntity(String.class);
				if (!responseContent.contentEquals(EXPECTED_RESPONSE)) {
					return constructHealthDownWithReason(
							String.format("%s : %s", "Erro response obtained", responseContent));
				}
			}
			return "{\"status\":\"UP\",\"greetingHealthCheck\":{\"status\":\"UP\"},\"diskSpace\":{\"status\":\"UP\"}}";
		} catch (Exception e) {
			return constructHealthDownWithReason(e.getLocalizedMessage());
		} finally {
			if (client != null) {
				client.close();
			}
		}
	}

	private String constructHealthDownWithReason(String reason) {
		return "{\"status\":\"DOWN\",\"greetingHealthCheck\":{\"reason\":\"+ reason + \"}}";
	}

}
