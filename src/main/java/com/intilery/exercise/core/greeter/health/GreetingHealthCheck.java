package com.intilery.exercise.core.greeter.health;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class GreetingHealthCheck implements HealthIndicator {

	private Client client;
	private final String GREETING_URL = "http://localhost:8080/hello-world";
	private final String EXPECTED_RESPONSE = "{\"greeting\":\"Hello World!\"}";

	@Override
	public Health health() {
		try {
			client = ClientBuilder.newClient();
			Invocation invocation = client.target(GREETING_URL).request(MediaType.APPLICATION_JSON_TYPE).buildGet();
			Response response = invocation.invoke();
			if (response == null) {
				return constructHealthDownWithReason("HealthCheck response is null");
			} else if (Status.OK.getStatusCode() != response.getStatus()) {
				return constructHealthDownWithReason(
						String.format("%s : %s", "Error status code is", response.getStatus()));
			} else {
				String responseContent = response.readEntity(String.class);
				if (!responseContent.contentEquals(EXPECTED_RESPONSE)) {
					return constructHealthDownWithReason(
							String.format("%s : %s", "Erro response obtained", responseContent));
				}
			}
			return Health.up().build();
		} catch (Exception e) {
			return constructHealthDownWithException(e);
		} finally {
			if (client != null) {
				client.close();
			}
		}
	}

	private Health constructHealthDownWithReason(String reason) {
		return Health.down().withDetail("reason", reason).build();
	}

	private Health constructHealthDownWithException(Exception e) {
		return Health.down().withException(e).build();
	}
}
