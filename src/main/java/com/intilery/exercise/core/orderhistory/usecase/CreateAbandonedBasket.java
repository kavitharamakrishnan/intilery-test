package com.intilery.exercise.core.orderhistory.usecase;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.intilery.exercise.core.ecommerce.repository.UserGraphRepository;
import com.intilery.exercise.core.greeter.domain.Basket;
import com.intilery.exercise.core.greeter.domain.CustomerBasket;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

@Component
public class CreateAbandonedBasket {

	@Autowired
	private UserGraphRepository userGraphRepository;

	private final String CHECKOUT_LABEL = "check out";
	private final String BASKET_LABEL = "add to basket";
	private final String VISIT_LABEL = "visit";
	private final String CREATED_AT = "createdAt";
	private final String PRODUCT_NAME = "name";
	private final String PRODUCT_PRICE = "price";
	private final String PRODUCT_QUANTITY = "qty";
	private final String PRODUCT_IMAGE = "image";

	public CustomerBasket getAbandonedBasket(String emailId) {
		if (isInValidEmailId(emailId)) {
			throw new IllegalArgumentException("Email Id should not be empty");
		}
		CustomerBasket customerBasket = new CustomerBasket();
		Vertex customerVertex = userGraphRepository.getForUser(emailId);
		customerBasket.withEmailId(emailId);
		customerBasket.getBasket().addAll(getAllProductsEligible(customerVertex));
		return customerBasket;
	}

	public List<Basket> getAllProductsEligible(Vertex customerVertex){
		List<Basket> products = new ArrayList<Basket>();
		Edge lastCheckOutOrderEdge = getLastCheckOutOrderEdge(customerVertex);
		Iterator<Vertex> visitIterator = customerVertex.getVertices(Direction.OUT, VISIT_LABEL).iterator();
		while (visitIterator.hasNext()) {
			Vertex visited = visitIterator.next();
			Iterator<Edge> productEdgeIterator = visited.getEdges(Direction.OUT, BASKET_LABEL).iterator();
			while (productEdgeIterator.hasNext()) {
				Edge productEdge = productEdgeIterator.next();
				DateTime productDateTime = productEdge.getProperty(CREATED_AT);
				if (productDateTime.isAfter(lastCheckOutOrderEdge.getProperty(CREATED_AT))) {
					Vertex productVertex = productEdge.getVertex(Direction.IN);
					Basket basket = new Basket();
					basket.withImage(productVertex.getProperty(PRODUCT_IMAGE)).withName(productVertex.getProperty(PRODUCT_NAME))
						  .withPrice(productVertex.getProperty(PRODUCT_PRICE));
					if (products.contains(basket)) {
						Basket existingBasket = products.get(products.indexOf(basket));
						existingBasket.withQuantity(existingBasket.getQuantity() + (int) productEdge.getProperty(PRODUCT_QUANTITY));
					}else {
						basket.withQuantity(productEdge.getProperty(PRODUCT_QUANTITY));
						products.add(basket);
					}
				}
			}
		}
		return products;
	}

	public Edge getLastCheckOutOrderEdge(Vertex customerVertex) {
		Edge lastCheckOutEdge = null;
		Iterator<Vertex> visitVertexIterator = customerVertex.getVertices(Direction.OUT, VISIT_LABEL).iterator();
		while (visitVertexIterator.hasNext()) {
			Vertex visited = visitVertexIterator.next();
			Iterator<Edge> orderEdgeIterator = visited.getEdges(Direction.OUT, CHECKOUT_LABEL).iterator();
			while (orderEdgeIterator.hasNext()) {
				Edge orderEdge = orderEdgeIterator.next();
				DateTime orderDateTime = orderEdge.getProperty(CREATED_AT);
				if (lastCheckOutEdge == null) {
					lastCheckOutEdge = orderEdge;
				}else if (orderDateTime.isAfter(lastCheckOutEdge.getProperty(CREATED_AT))) {
					lastCheckOutEdge = orderEdge;
				}
			}
		}
		return lastCheckOutEdge;
	}

	public UserGraphRepository getUserGraphRepository() {
		return userGraphRepository;
	}

	public void setUserGraphRepository(UserGraphRepository userGraphRepository) {
		this.userGraphRepository = userGraphRepository;
	}

	public boolean isInValidEmailId(String emailId) {
		return StringUtils.isBlank(emailId);
	}

}
