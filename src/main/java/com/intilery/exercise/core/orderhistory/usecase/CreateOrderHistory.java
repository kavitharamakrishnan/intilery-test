package com.intilery.exercise.core.orderhistory.usecase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.intilery.exercise.core.ecommerce.repository.UserGraphRepository;
import com.intilery.exercise.core.greeter.domain.CheckOutOrder;
import com.intilery.exercise.core.greeter.domain.Line;
import com.intilery.exercise.core.greeter.domain.Order;
import com.intilery.exercise.core.greeter.domain.OrderHistory;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

@Component
public class CreateOrderHistory {

	@Autowired
	private UserGraphRepository userGraphRepository;

	private final String VISIT_LABEL = "visit";
	private final String BASKET_LABEL = "add to basket";
	private final String CHECKOUT_LABEL = "check out";
	private final String ORDER_ID = "orderID";
	private final String CREATED_AT = "createdAt";
	private final String PRODUCT_NAME = "name";
	private final String PRODUCT_PRICE = "price";
	private final String PRODUCT_QUANTITY = "qty";
	private final String PRODUCT_IMAGE = "image";

    public OrderHistory getOrderHistory(String emailId) {
    		if (isInValidEmailId(emailId)) {
			throw new IllegalArgumentException("Email Id should not be empty");
		}
        Vertex customerVertex = userGraphRepository.getForUser(emailId);
        return createAllOrdersHistory(emailId, customerVertex);
    }

    public OrderHistory getOrderHistory(String emailId, String orderId) {
    		if (isInValidEmailId(emailId)) {
			throw new IllegalArgumentException("Email Id should not be empty");
		}
        Vertex customerVertex = userGraphRepository.getForUser(emailId);
        return createSingleOrderHistory(emailId, customerVertex, orderId);
    }

    public OrderHistory createSingleOrderHistory(String emailId, Vertex customerVertex, String orderId) {
    		final OrderHistory orderHistory = new OrderHistory();
    		CheckOutOrder checkOutOrder = getMatchingAndItsPreviousOrder(customerVertex, orderId);
    		orderHistory.withEmailId(emailId);
    		Order order = new Order();
    		order.withOrderId(checkOutOrder.getCurrentCheckOutOrder().getProperty(ORDER_ID));
    		getDescendingSortedVisitVertices(customerVertex).iterator().forEachRemaining(visit ->
    													constructCheckOrder(order, visit, checkOutOrder));
    		orderHistory.getOrders().add(order);
    		return orderHistory;
    }

    public OrderHistory createAllOrdersHistory(String emailId, Vertex customerVertex) {
    		final OrderHistory orderHistory = new OrderHistory();
    		orderHistory.withEmailId(emailId);
    		getDescendingSortedVisitVertices(customerVertex).iterator().forEachRemaining(visit ->
    									orderHistory.getOrders().addAll(constructCheckOutOrders(visit)));
    		return orderHistory;

    }

    public List<Order> constructCheckOutOrders(Vertex visit) {
    		List<Order> orders = new ArrayList<Order>();
    		visit.getVertices(Direction.OUT, CHECKOUT_LABEL).forEach(order ->
    															  orders.add(new Order().withOrderId(order.getProperty(ORDER_ID))));
    		return orders;
    }

	public void constructCheckOrder(Order order, Vertex visit, CheckOutOrder checkOutOrder) {
		Iterator<Edge> productEdgeIterator = visit.getEdges(Direction.OUT, BASKET_LABEL).iterator();
		while (productEdgeIterator.hasNext()) {
			Edge productEdge = productEdgeIterator.next();
			Vertex productVertex = productEdge.getVertex(Direction.IN);
			DateTime productDateTime = productEdge.getProperty(CREATED_AT);
			Vertex previousCheckOutOrder = checkOutOrder.getPreviousCheckOutOrder();
			if ( (previousCheckOutOrder == null ||
				  getCheckOutOrderCreatedDateTime(previousCheckOutOrder).isBefore(productDateTime)) &&
				  getCheckOutOrderCreatedDateTime(checkOutOrder.getCurrentCheckOutOrder()).isAfter(productDateTime) ) {
					Line line = new Line();
					line.withName(productVertex.getProperty(PRODUCT_NAME)).withPrice(productVertex.getProperty(PRODUCT_PRICE))
						.withImage(productVertex.getProperty(PRODUCT_IMAGE));
					if (order.getLines().contains(line)) {
						Line existingLine = order.getLines().get(order.getLines().indexOf(line));
						existingLine.withQuantity(existingLine.getQuantity() + (int) productEdge.getProperty(PRODUCT_QUANTITY));
					}else {
						line.withQuantity(productEdge.getProperty(PRODUCT_QUANTITY));
						order.getLines().add(line);
					}
			}
		}
	}

	private DateTime getCheckOutOrderCreatedDateTime(Vertex checkOutOrder) {
		DateTime dateTime = checkOutOrder.getEdges(Direction.IN, CHECKOUT_LABEL).iterator().next().getProperty(CREATED_AT);
		return dateTime;
	}

	public CheckOutOrder getMatchingAndItsPreviousOrder(Vertex customerVertex, String orderId) {
		Vertex lastCheckOutOrder = null;
		Vertex matchingOrder = null;
		Iterator<Vertex> visitIterator = getDescendingSortedVisitVertices(customerVertex).iterator();
		while (visitIterator.hasNext() && lastCheckOutOrder == null ) {
			Vertex visit = visitIterator.next();
			Iterator<Vertex> orderIterators = visit.getVertices(Direction.OUT, CHECKOUT_LABEL).iterator();
			while (orderIterators.hasNext()) {
				Vertex order = orderIterators.next();
				if (order.getProperty(ORDER_ID).equals(orderId)) {
					matchingOrder = order;
				} else if (matchingOrder != null && matchingOrder != order) {
					lastCheckOutOrder = order;
					break;
				}
			}
		}
		return new CheckOutOrder().withCurrentCheckOutOrder(matchingOrder).withPreviousCheckOutOrder(lastCheckOutOrder);
	}

	public List<Vertex> getDescendingSortedVisitVertices(Vertex customerVertex){
		final List<Vertex> sortedVisitVertexList = new ArrayList<Vertex>();
		customerVertex.getVertices(Direction.OUT, VISIT_LABEL).forEach(visitVertex -> sortedVisitVertexList.add(visitVertex));
		Collections.sort(sortedVisitVertexList, new Comparator<Vertex>() {
			@Override
			public int compare(Vertex o1, Vertex o2) {
				return ((DateTime) o2.getProperty("sessionStarted")).compareTo(o1.getProperty("sessionStarted"));
			}
		});
		return sortedVisitVertexList;
    }

	public UserGraphRepository getUserGraphRepository() {
		return userGraphRepository;
	}

	public void setUserGraphRepository(UserGraphRepository userGraphRepository) {
		this.userGraphRepository = userGraphRepository;
	}

	public boolean isInValidEmailId(String emailId) {
		return StringUtils.isBlank(emailId);
	}

}
