package com.intilery.exercise.web.facade;

import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.intilery.exercise.core.greeter.domain.CustomerBasket;
import com.intilery.exercise.core.orderhistory.usecase.CreateAbandonedBasket;

@RestController
@RequestMapping("/basket")
public class BasketResource {
    private final CreateAbandonedBasket abandonedBasket;

    @Autowired
    public BasketResource(CreateAbandonedBasket abandonedBasket) {
        this.abandonedBasket = abandonedBasket;
    }

    @RequestMapping(path="/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomerBasket createAbandonedBasket(@PathVariable("email") final Optional<String> emailId) {
        return abandonedBasket.getAbandonedBasket(emailId.orElse(StringUtils.EMPTY));
    }

}
