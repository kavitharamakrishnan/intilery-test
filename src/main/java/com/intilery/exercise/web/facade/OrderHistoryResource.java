package com.intilery.exercise.web.facade;

import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.intilery.exercise.core.greeter.domain.OrderHistory;
import com.intilery.exercise.core.orderhistory.usecase.CreateOrderHistory;

@RestController
@RequestMapping("/orders")
public class OrderHistoryResource {
    private final CreateOrderHistory createOrderHistory;

    @Autowired
    public OrderHistoryResource(CreateOrderHistory createOrderHistory) {
        this.createOrderHistory = createOrderHistory;
    }

    @RequestMapping(path="/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    public OrderHistory createOrderHistory(@PathVariable("email") final Optional<String> emailId) {
        return createOrderHistory.getOrderHistory(emailId.orElse(StringUtils.EMPTY));
    }

    @RequestMapping(path="/{email}/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public OrderHistory getOrder(@PathVariable("email") final Optional<String> emailId, @PathVariable("orderId") final Optional<String> orderId) {
        return createOrderHistory.getOrderHistory(emailId.orElse(StringUtils.EMPTY), orderId.orElse(StringUtils.EMPTY));
    }
}
