package com.intilery.exercise.web.facade;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.intilery.exercise.core.processing.usecase.DoProcessing;

@RestController
@RequestMapping("/process")
public class ProcessingResource {
    private final DoProcessing doProcessing;

    @Autowired
    public ProcessingResource(DoProcessing doProcessing) {
        this.doProcessing = doProcessing;
    }

    @SuppressWarnings("rawtypes")
	@RequestMapping(value = "/single", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity processSingle() {
		//As a seperate process.
		//Runtime.getRuntime().exec("java DoProcessing");

    		//WithIn same process
		ExecutorService executors = Executors.newFixedThreadPool(1);
		executors.submit(new Runnable() {
			@Override
			public void run() {
				doProcessing.doLongRunningSerialProcess();
			}
		});
		executors.shutdown();
        return ResponseEntity.accepted().build();
    }

    @SuppressWarnings("rawtypes")
	@RequestMapping(value = "/concurrent/{name}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity processMany(@PathVariable("name") String name) {
        Integer result = doProcessing.doMultiThreadedProcess(name);
        return ResponseEntity.ok(result);
    }
}
