package com.intilery.exercise.core.greeter.usecase;

import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.intilery.exercise.core.ecommerce.repository.UserGraphRepository;
import com.intilery.exercise.core.greeter.domain.CustomerBasket;
import com.intilery.exercise.core.orderhistory.usecase.CreateAbandonedBasket;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;


public class CreateAbandoneBasketTest {
	
	private CreateAbandonedBasket abandonedBasket = new CreateAbandonedBasket();
	private 	UserGraphRepository userGraphRepository = new UserGraphRepository();
	
	@Before
	public void setUp() {
		abandonedBasket.setUserGraphRepository(userGraphRepository);
	}

	@Test
	public void shouldReturnTheCorrectLastOrder() {
		Edge lastCheckOutOrderEdge = abandonedBasket.getLastCheckOutOrderEdge(userGraphRepository.getForUser("tom@example.com"));
		assertEquals(new DateTime(2013, 9, 4, 16, 25), lastCheckOutOrderEdge.getProperty("createdAt"));
		assertEquals("ABC2", lastCheckOutOrderEdge.getVertex(Direction.IN).getProperty("orderID"));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentExceptionForEmptyEmailId() {
		abandonedBasket.getAbandonedBasket("");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentExceptionForNullEmailId() {
		abandonedBasket.getAbandonedBasket(null);
	}

}
