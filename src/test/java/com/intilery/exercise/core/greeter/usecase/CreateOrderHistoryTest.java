package com.intilery.exercise.core.greeter.usecase;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import com.intilery.exercise.core.ecommerce.repository.UserGraphRepository;
import com.intilery.exercise.core.greeter.domain.CheckOutOrder;
import com.intilery.exercise.core.greeter.domain.OrderHistory;
import com.intilery.exercise.core.orderhistory.usecase.CreateOrderHistory;
import com.tinkerpop.blueprints.Vertex;

public class CreateOrderHistoryTest {
	
	private 	UserGraphRepository userGraphRepository = new UserGraphRepository();
	private CreateOrderHistory orderHistory = new CreateOrderHistory();
	private Vertex customerVertex = userGraphRepository.getForUser("kavitha@example.com");
	
	@Before
	public void setUp() {
		orderHistory.setUserGraphRepository(userGraphRepository);
	}

	@Test
	public void shouldReturnCorrectlySortedVertices() {
		orderHistory.setUserGraphRepository(userGraphRepository);
		List<Vertex> descendingSortedVisitVertices = orderHistory.getDescendingSortedVisitVertices(customerVertex);
		assertEquals(new DateTime(2013, 9, 4, 16 ,22), descendingSortedVisitVertices.get(0).getProperty("sessionStarted"));
		assertEquals(new DateTime(2013, 9, 4, 11, 15), descendingSortedVisitVertices.get(1).getProperty("sessionStarted"));
		assertEquals(new DateTime(2013, 9, 1, 12, 10), descendingSortedVisitVertices.get(2).getProperty("sessionStarted"));
	}
	
	@Test
	public void shouldReturnCorrectMatchingAndLastOrder() {
		CheckOutOrder matchingAndItsPreviousOrder = orderHistory.getMatchingAndItsPreviousOrder(customerVertex,"ABC2");
		assertEquals("ABC2",matchingAndItsPreviousOrder.getCurrentCheckOutOrder().getProperty("orderID"));
		assertEquals("ABC1",matchingAndItsPreviousOrder.getPreviousCheckOutOrder().getProperty("orderID"));
	}
	
	@Test
	public void shouldReturnCorrectSingleOrderHistory() {
		OrderHistory singleOrderHistory = orderHistory.createSingleOrderHistory("kavitha@example.com", customerVertex, "ABC1");
		assertEquals("kavitha@example.com",singleOrderHistory.getEmailId());
		assertEquals(1,singleOrderHistory.getOrders().size());
		assertEquals("Sourpuss Maiden of the Sea Cushion",singleOrderHistory.getOrders().get(0).getLines().get(0).getName());
	}
	
	@Test
	public void shouldReturnAllOrdersCorrectly() {
		OrderHistory allOrders =  orderHistory.createAllOrdersHistory("kavitha@example.com", customerVertex);
		assertEquals("kavitha@example.com",allOrders.getEmailId());
		assertEquals(2, allOrders.getOrders().size());
		assertEquals("ABC2", allOrders.getOrders().get(0).getOrderId());
		assertEquals("ABC1", allOrders.getOrders().get(1).getOrderId());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentExceptionForEmptyEmailId() {
		orderHistory.getOrderHistory("");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentExceptionForNullEmailId() {
		orderHistory.getOrderHistory(null);
	}

}