package features;

import com.intilery.exercise.web.ExerciseApplication;
import features.support.Timer;
import org.apache.commons.collections.Bag;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExerciseApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public abstract class SpringBootBaseAcceptanceTest {
    private static final Logger logger = LoggerFactory.getLogger(SpringBootBaseAcceptanceTest.class);
    private final String SERVER_URL = "http://localhost";

    private String baseUrl;
    private String adminUrl;
    private Client client;
    private ResponseWrapper lastResponse;
    protected Timer timer = new Timer();

    public SpringBootBaseAcceptanceTest() {
        baseUrl = SERVER_URL + ":" + 8080;
        adminUrl = SERVER_URL + ":" +  8081;
        client = ClientBuilder.newClient();
    }

    public void shutdown() {
        client.close();
    }

    public String getLastResponse() {
        return lastResponse.getContent();
    }

    public int getLastResponseStatus() {
        return lastResponse.getStatus();
    }

    public void getEndpoint(String path) {
        get(baseUrl + path);
    }
    
    public void getHealthCheck() {
        get(adminUrl + "/actuator/greeterHealth");
    }

    public void process() {
        post(baseUrl + "/process/single", null);
    }

    public void concurrentProcess() {
        post(baseUrl + "/process/concurrent/fred", null);
    }

    private void post(String url, Entity entity) {
        getResponse(client.target(url).request(MediaType.APPLICATION_JSON_TYPE).buildPost(entity));
    }

    private void get(String url) {
        getResponse(client.target(url).request(MediaType.APPLICATION_JSON_TYPE).buildGet());
    }

    private void getResponse(Invocation invocation) {
        lastResponse = new ResponseWrapper(invocation.invoke());
    }

    private class ResponseWrapper {
        private final int status;
        private final String content;

        ResponseWrapper(Response response) {
            status = response.getStatus();
            content = response.readEntity(String.class);
        }

        int getStatus() {
            return status;
        }

        String getContent() {
            return content;
        }
    }
}
