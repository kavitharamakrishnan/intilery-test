package features.stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.SpringBootBaseAcceptanceTest;
import org.apache.commons.io.IOUtils;
import org.hamcrest.MatcherAssert;
import org.skyscreamer.jsonassert.JSONAssert;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ApplicationSteps extends SpringBootBaseAcceptanceTest {

	@Given("^a running application$")
	public void a_running_application() {
	}

	@When("^I check the application health$")
	public void i_check_the_application_health() {
		getHealthCheck();
	}

	@When("^I GET \"([^\"]*)\"$")
	public void I_GET(String url) {
		getEndpoint(url);
	}

	/*
	 * Acutator 1.x details are different from Acutator 2.X details
	 * Below test case will pass by health indicator with removed comma
	 */
	 /* @Then("^I see status \"([^\"]*)\" for \"([^\"]*)\"$") public void
	 I_see_status(String expectedResponse, String property) {
	 assertThat(getLastResponse().contains(String.format(
	 * "\"%s\":{\"status\":\"%s\"},", property, expectedResponse)), is(true)); }
	 */

	@Then("^I see status \"([^\"]*)\" for \"([^\"]*)\"$")
	public void I_see_status_greeterHealth(String expectedResponse, String property) {
		assertThat(
				getLastResponse().contains(String.format(",\"%s\":{\"status\":\"%s\"},", property, expectedResponse)),
				is(true));
	}

	@Then("^it has status (\\d+)$")
	public void it_has_status(int expectedStatus) {
		assertThat(getLastResponseStatus(), is(expectedStatus));
	}

	@Then("^I see JSON like \"([^\"]*)\"$")
	public void I_see_JSON_like(String filename) throws Throwable {
		JSONAssert.assertEquals(expectedJSON(filename), getLastResponse(), false);
	}

	@When("^I POST (\\d+) serial processes$")
	public void I_POST_serial_processes(int count) {
		timer.start();
		while (count-- > 0) {
			process();
		}
	}

	@Then("^I get a valid response for each one$")
	public void I_get_a_valid_response_for_each_one() {
		MatcherAssert.assertThat(getLastResponseStatus() > 199, is(true));
		MatcherAssert.assertThat(getLastResponseStatus() < 300, is(true));
	}

	@Then("^it takes less than (\\d+) second$")
	public void it_takes_less_than_second(int interval) {
		MatcherAssert.assertThat(timer.stop() < interval, is(true));
	}

	@When("^I POST (\\d+) concurrent requests$")
	public void i_POST_concurrent_requests(int count) throws Throwable {
		ExecutorService executorService = Executors.newFixedThreadPool(6);
		while (count-- > 0) {
			executorService.submit(this::concurrentProcess);
		}
		executorService.shutdown();
		executorService.awaitTermination(25, TimeUnit.SECONDS);
	}

	@Then("^I get the count (\\d+) back$")
	public void i_get_the_count_back(int count) {
		concurrentProcess();
		MatcherAssert.assertThat(Integer.parseInt(getLastResponse()) - 1, is(count));
	}

	private String expectedJSON(String filename) {
		InputStream in = this.getClass().getResourceAsStream(String.format("/json/%s.json", filename));
		StringWriter writer = new StringWriter();
		try {
			IOUtils.copy(in, writer, "UTF-8");
		} catch (IOException e) {
			throw new RuntimeException("Can't read JSON file " + filename);
		}
		return writer.toString();
	}
}
